<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;


class SumHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $queryParams = $request->getQueryParams();
        if(isset($queryParams['num1']) and isset($queryParams['num2'])) {
            $num1 = $queryParams['num1'];
            $num2 = $queryParams['num2'];
            if(is_numeric($num1) and is_numeric($num2)) {
                return new JsonResponse(['sum' => $num1+$num2], 200);
            }
        }
        return new JsonResponse(["Error" => "Input value error: num1 and num2 must exist and must be numbers."], 400);
    }
}
