<?php

declare(strict_types=1);

namespace AppTest\Handler;

use App\Handler\SumHandler;
use Laminas\Diactoros\Response\JsonResponse;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

use function json_decode;

class SumHandlerTest extends TestCase
{
    public function testResponse(): void
    {
        $sumHandler = new SumHandler();
        $response    = $sumHandler->handle(
            $this->createMock(ServerRequestInterface::class)
        );

        $json = json_decode((string) $response->getBody());

        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertTrue(isset($json->ack));
    }
}
